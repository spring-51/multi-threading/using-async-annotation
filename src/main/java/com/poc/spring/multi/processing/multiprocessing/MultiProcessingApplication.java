package com.poc.spring.multi.processing.multiprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiProcessingApplication {
    public static void main(String[] args) {
        SpringApplication.run(MultiProcessingApplication.class, args);
    }
}
