package com.poc.spring.multi.processing.multiprocessing.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@EnableAsync // it uses Thread pool
public class AsyncConfig {

    @Bean(name = "taskExecutor")
    public Executor taskExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2); // defining thread pool core capacity /size
        executor.setMaxPoolSize(2); // defining thread pool capacity /size
        executor.setQueueCapacity(100); // this number of task can wait in blocking queue
        executor.setThreadNamePrefix("userThread-"); // add prefix to running thread
        return executor;
    }
}
